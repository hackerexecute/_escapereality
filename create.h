#include <string>
#include <vector>
#include "date.h"

class Create
{
    public:
    Create() {}
    ~Create() {}

    std::string createName();
    void greetPlayer(std::string name);

    Date createBirthdate();
    std::vector<int> parseDateToVector(std::string date);
    int convert(std::string str);
    Date setBirthDate(std::vector<int> dates);

    
};
