/************************************************************

Description: The "Player" acts as the virtualized user/person
             whom is using the program. The intent is to 
             copy as much of the user into the program as
             possible.

Author: Cristian Canedo
              
Environment: Visual Studio Code / MinGW G++

Notes: Name of object subject to change.

Revisions:
Name               | Date        | Change
------------------------------------------------------------
Cristian Canedo    | 11 Mar 2018 | Add header comment
************************************************************/

#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <vector>
#include "date.h"
#include "attributes.h"
#include "aspects/hobby.h"
#include "aspects/skill.h"
#include "aspects/emotion.h"
#include "aspects/thought.h"

class Player
{
    private:
    std::string _name;
    Date _birthdate;
    int _age, _weight, _height;
    sex _sex;
    color _hairColor;
    std::vector<Hobby> _hobbies;
    std::vector<Skill> _skills;
    std::vector<Thought> _thoughts;
    std::vector<Emotion> _emotions;
    
    public:
    Player() {}
    ~Player() {}
    
    /**********Getter Functions**********/
    std::string getName() const { return _name; }
    Date getBirthdate() const { return _birthdate; }
    int getAge() const { return _age; }
    int getWeight() const { return _weight; }
    int getHeight() const { return _height; }
    sex getSex() const { return _sex; }
    color getColor() const { return _hairColor; }
    std::vector<Hobby> getHobbies() const { return _hobbies; }
    std::vector<Skill> getSkills() const { return _skills; }
    std::vector<Thought> getThoughts() const { return _thoughts; }
    std::vector<Emotion> getEmotions() const { return _emotions; }
    /**********Getter Functions**********/
    
    /**********Setter Functions**********/
    void setName(std::string n) { this->_name = n; }
    void setBirthdate(const Date &b) { this->_birthdate = b; }
    void setAge();
    void setWeight(int w) { this->_weight = w; }
    void setHeight(int h) { this->_height = h; }
    void setSex(sex s) { this->_sex = s; }
    void setColor(color c) { this->_hairColor = c; }
    void addHobby(const Hobby &h) { this->_hobbies.push_back(h); }
    void addSkill(const Skill &s) { this->_skills.push_back(s); }
    void addThought(const Thought &t) { this->_thoughts.push_back(t); }
    void addEmotion(const Emotion &e) { this->_emotions.push_back(e); }
    /**********Setter Functions**********/
    
    /* Initiates the creation of the player */
    void initCreation();
};

#endif // PLAYER_H
