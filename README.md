# _escapeReality-
In all seriousness I am creating this project to serve as a virtual escape from reality. I want to project myself or any user into this program, and have the program remember all details of the user entered.

I want to be able to open this application and feel a sense of relief to now be within a virtual world able to do anything, forgetting the issues of the real world for just a moment. Similar to that feeling of playing a text-based adventure MUD.

I'm not entirely sure where this project is headed yet. I could add an "AI" chatbot similar to ELIZA. I could add an entire virtual world with the ability to do anything.  Regardless of where this is heading, creating this program already serves as an escape from reality by itself.
