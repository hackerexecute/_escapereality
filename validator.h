#ifndef VALIDATOR_H
#define VALIDATOR_H

#include <string>
#include <vector>

class Validator
{
    public:
    Validator() {}
    ~Validator() {}

    static bool validateName(std::string name);
    static bool validateFormat(std::string birthdate);
};

#endif // VALIDATOR_H