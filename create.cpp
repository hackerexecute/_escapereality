#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "validator.h"
#include "create.h"

std::string Create::createName()
{
    std::string input;
    bool isValid = false;
    std::cout << "\nHello. Who are you?\n";
    
    do {
        std::cout << "Name: ";
        std::getline(std::cin, input);

        isValid = Validator::validateName(input);

        // Future: Check if name already exists

        if (!isValid) {
            std::cout << "You must enter a name.\n" << std::endl;
            continue;
        }
        
        break;
    } while (true);
    
    return input;
}

void Create::greetPlayer(std::string name)
{
    std::cout << "\nNice to meet you, " << name << "!\n\n";
}

Date Create::createBirthdate()
{
    std::string input;
    std::vector<int> dates(3);
    bool isDateValid = false;

    do {
        std::cout << "Enter your birthdate (MM/DD/YYYY): ";
        std::getline(std::cin, input);

        isDateValid = Validator::validateFormat(input); // Ensure date is formatted and appropriate

        if (!isDateValid) {
            std::cout << "Invalid date format. Try again.\n\n";
            continue;
        }
        else {
            dates = parseDateToVector(input);
            Date birthDate = setBirthDate(dates); // Set date
            return birthDate;
        }
    } while (true);
}

std::vector<int> Create::parseDateToVector(std::string date)
{   
    std::vector<int> dates(3); // {MM, DD, YYYY}
    int index = 0;
    size_t len = date.length();

    for (int i = 0; i < len; i++) {
        if (date[i] == '/' && i == 2) { // Set MM on first forward slash
            int mon = convert(date.substr(i - 2, i)); // Convert substring to integer
            dates[index] = mon;
            index++;
        }
        else if (date[i] == '/' && i == 5) { // Set DD and YYYY on second forward slash
            int day = convert(date.substr(i - 2, i));
            dates[index] = day;
            index++;

            int year = convert(date.substr(i + 1, 4));
            dates[index] = year;
            break;
        }
    }

    return dates;
}

int Create::convert(std::string str)
{
    std::stringstream conv(str);
    int num;
    conv >> num;
    return num;
}

Date Create::setBirthDate(std::vector<int> dates)
{
    int mon = dates[0];
    int day = dates[1];
    int year = dates[2];

    Date date(mon, day, year);
    return date;
}


