#ifndef DATE_H
#define DATE_H

#include <string>

struct Date
{
    private:
    int _mon, _day, _year;
    
    public:
    Date() {}
    Date(int mon, int day, int year)
        :_mon(mon), _day(day), _year(year) {}
    ~Date() {}

    void setMonth(int mon) { this->_mon = mon; }
    void setDay(int day) { this->_day = day; }
    void setYear(int year) { this->_year = year; }
    int getMonth() const { return _mon; }
    int getDay() const { return _day; }
    int getYear() const { return _year; }
};

#endif // DATE_H