#include <iostream>
#include <ctime>
#include <exception>
#include "player.h"
#include "create.h"

void Player::setAge()
{
    int age;
    int month = this->_birthdate.getMonth();
    int year = this->_birthdate.getYear();
    int day = this->_birthdate.getDay();

    std::time_t curr = std::time(nullptr);
    tm *timePtr = std::localtime(&curr);   // Getting current month, day, year
    
    int c_month = timePtr->tm_mon;
    int c_year = timePtr->tm_year + 1900;
    int c_day = timePtr->tm_mday;

    age = c_year - year;

    // If birth month is the current month, check day
    if (c_month == month) {
        age += c_day >= day ? 1 : 0;
    }
    else if (c_month > month) {
        age++;
    }

    this->_age = age;
}

void Player::initCreation()
{
    Create helper;
    
    std::string n = helper.createName();
    setName(n);

    helper.greetPlayer(getName());

    Date b = helper.createBirthdate();
    setBirthdate(b);


}
