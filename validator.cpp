#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <regex>
#include "validator.h"

bool Validator::validateName(std::string name)
{
    if (name.length() <= 0) {
        return false;
    }
    return true;
}

bool Validator::validateFormat(std::string birthdate)
{
    std::regex reg("((0|1)\\d{1})\\/((0|1|2)\\d{1}|3\\d{01})\\/((19|20)\\d{2})");
    if (!std::regex_match(birthdate, reg)) {
        return false;
    }
    return true;
}

